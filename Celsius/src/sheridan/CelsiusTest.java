package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelsiusTest {

	// tests to ensure test passes
	@Test
	public void testConvertToFahrenheitRegular() {
		int celsius = sheridan.celsius.fromFahrenheit(212);
		assertTrue("Unable to convert fahrenheit to celsius", celsius == 100);
	}
	
	/*
	 * // tests if farenheit gets converted to celsius when entering a double
	 * 
	 * @Test (expected = NumberFormatException.class) public void
	 * testConvertToFarenheitException() { double celsius =
	 * sheridan.celsius.fromFahrenheit(221.0);
	 * fail("Farenheit needs to be an integer"); }
	 */
	
	@Test
	public void testConvertToFarenheitBoundaryIn() {
		int celsius = sheridan.celsius.fromFahrenheit(-255);
		assertTrue("Unable to convert to fahrenheit to celsius", celsius == -159);
	}
	
	@Test
	public void testConvertToFarenheitBoundaryOut() {
		int celsius = sheridan.celsius.fromFahrenheit(1000000000);
		assertFalse("Parameter is too large for conversion", celsius == 555555538);
	}
	
	
	
	
	

}
